module.exports = {
    // The Process env in set in the package.json "scripts" object
    inProduction: (process.env.NODE_ENV === "production"),
    useSourceMaps: !(process.env.NODE_ENV === "production"),

    // The files to watch when compiling
    input: [
        "./src/js/app.js",
        "./src/sass/app.scss"
    ],

    // Output files
    output: {
        js: {
            dir: "./dist",
            filename: "app"
        },
        css: {
            dir: "./dist",
            filename: "app"
        }
    },

    // Plugin options
    autoprefixer: "", // "last 4 versions"
};