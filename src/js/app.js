import axios from 'axios';
import ExampleComponent from './components/Example.vue';
import Vue from 'vue';

Vue.component('example', ExampleComponent);

new Vue({
    el: "#app"
});