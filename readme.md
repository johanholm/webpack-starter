## Webpack config

Author: Johan Holm

### Installation

`yarn` or `npm install`

### Settings

You will find the settings in `config.js`

### Usage

* `yarn run dev` or `npm run dev`

* `yarn run watch` or `npm run watch`

* `yarn run production` or `npm run production`

### Features

* Babel es6

* Vue.js and Vue templates (.vue extension)

* Require Modules (standard webpack)

* File watcher

* Minification

* Sourcemaps

* Scss and Sass support

* Autoprefixer for css