let path = require("path");
let webpack = require("webpack");
let config = require("./config");
let autoprefixer = require("autoprefixer");
let ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    devtool: config.useSourceMaps ? "#inline-source-map" : false,
    entry: config.input,
    output: {
        path: path.resolve(__dirname, config.output.js.dir),
        filename: `${config.output.js.filename}.js`
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                url: false,
                                sourceMap: config.useSourceMaps
                            }
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                plugins: (loader) => [
                                    autoprefixer(config.autoprefixer)
                                ],
                                sourceMap: config.useSourceMaps
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: config.useSourceMaps
                            }
                        }
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
                options: {
                    sourceMap: config.useSourceMaps,
                    extract: config.inProduction
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin(`${path.relative(path.resolve(__dirname, config.output.js.dir), path.resolve(__dirname, config.output.css.dir))}/${config.output.css.filename}.css`),

        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: config.inProduction ? '"production"' : "'development'"
            }
        }),

        new webpack.LoaderOptionsPlugin({
            minimize: config.inProduction
        }),
    ],
    resolve: {
        alias: {
            vue$: "vue/dist/vue.esm"
        }
    }
};

if(config.inProduction) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin()
    );
}